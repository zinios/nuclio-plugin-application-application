Release Notes
-------------
1.3.0
-----
Support for custom session handlers has been added. Session will now be created with the provider manager.

1.2.0
-----
Added support for mutli-environments. Place a .env file in the root with an environment string in it.  The same folder will be located in the config folder of the application and only config files from that folder will be loaded.

1.1.3
-----
* Added additional check for starting a session.

1.1.2
-----
* Fixed an issue which would cause an exception to be thrown if there was no templates folder. Templates should in fact be optional, and this patch corrects this bug.

1.1.1
-----
* Added missing "Dispatcher" return type to getDispatcher method.

1.1.0
-----
* Applications can now use getInstance() without the need to implement their own getInstance method.
* Fixed issue with template pathing.
* Depricated run(). init() should now be used instead. Also init() needs to be called manually.

1.0.2
-----
* Fixed template mapping issue.

1.0.1
-----
* Fixed PSR4 config.

1.0.0
-----
* Initial Release.