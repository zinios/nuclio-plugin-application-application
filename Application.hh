<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\application\application
{
	use nuclio\core\
	{
		loader\ClassLoader,
		ClassManager,
		EventManager,
		plugin\Plugin
	};
	use nuclio\
	{
		Nuclio,
		kernel\HttpKernel,
		exception\ExceptionBootstrapper
	};
	
	use nuclio\plugin\
	{
		config\Config,
		application\router\Router,
		debug\debug\Debug,
		http\request\Request,
		http\uri\URI,
		session\Session,
		session\CommonSessionInterface,
		database\datasource\manager\Manager as DataSourceManager,
		format\driver\template\twig\Twig,
		provider\manager\Manager as ProviderManager
	};
	use nuclio\helper\CryptHelper;
	use \ReflectionClass;
	
	<<__ConsistentConstruct>>
	abstract class Application extends Plugin
	{
		const MODE_PRODUCTION		='production';
		const MODE_DEVELOPMENT		='development';
		const DEFAULT_URI_BINDING	='/';
		const ENV_FILE				='.env';
		const DEFAULT_SESSION_TYPE	='fileSystem';
		
		/**
		 * 
		 * @depreciated
		 * @see Application::init()
		 */
		// abstract public function run():void;
		abstract public function init():void;
		
		private ?HttpKernel $kernel	=null;
		private ?Nuclio $nuclio	=null;
		
		private string $URIBinding;
		public Router $router;
		public Config $config;
		public Request $request;
		public URI $URI;
		private CommonSessionInterface $session;
		private Twig $twig;
		public ?CryptHelper $cryptHelper;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):this
		{
			$instance=ClassManager::getClassInstance(static::class,...$args);
			return ($instance instanceof static)?$instance:new static(...$args);
		}
		
		public function __construct(string $URIBinding=self::DEFAULT_URI_BINDING,?string $configPath=null)
		{
			parent::__construct();
			
			$this->URIBinding=$URIBinding;
			
			$this->initKernal();
			
			//Load based on environment.
			$envFile=ROOT_DIR.self::ENV_FILE;
			$env	=trim(file_get_contents($envFile));
			
			$classRef=lcfirst(get_class($this));
			if (is_null($configPath))
			{
				$parts			=explode('\\',$classRef);
				array_pop(&$parts);
				$pathPart		=implode('/',$parts);
				
				if (is_file($envFile))
				{
					$configPath	=realpath(ROOT_DIR.$pathPart.'/config/'.$env).'/';
				}
				else
				{
					$configPath	=realpath(ROOT_DIR.$pathPart.'/config').'/';
				}
			}
			else if (is_file($envFile))
			{
				$configPath=realpath($configPath.$env).'/';
			}
			//Init Config.
			$this->config	=Config::getInstance($configPath);
			
			//Crypt helper for config.
			$cryptConfig=$this->config->get('crypt');
			if (!is_null($cryptConfig))
			{
				$this->cryptHelper=CryptHelper::getInstance($cryptConfig);
			}
			
			//Init debuging.
			if ($this->config->get('application.debug'))
			{
				$debugger=Debug::getInstance($this->config);
			}
			
			$classMap	=include(PUBLIC_DIR.'../vendor/composer/autoload_classmap.php');
			$classMap	=new Map($classMap);
			$this->twig	=Twig::getInstance($this->config->get('controller.twig'));
			$reflection	=new ReflectionClass($this);
			$info		=new Map(pathinfo($reflection->getFilename()));
			
			if ($info->containsKey('dirname'))
			{
				if (is_dir($info['dirname'].'/template'))
				{
					$this->twig->addPath($info['dirname'].'/template');
				}
			}
			
			if ($classMap->containsKey(static::class))
			{
				$classPath			=$classMap->get(static::class);
				$pathParts			=explode('\\',static::class);
				array_shift(&$pathParts);
				$pathParts			=implode('/',$pathParts);
				$applicationPath	=str_replace
				(
					[$pathParts,'.hh'],
					'',
					$classPath
				);
				
				$templatePath			=$applicationPath .'template/';
				$pluginPath				=$applicationPath . 'plugin/';
				$widgetPath				=$pluginPath . 'widget/';
 				$customApptemplatePath	=$applicationPath."../"; 
 				
				if (is_dir($customApptemplatePath)) 
				{ 
					$this->twig->addPath($customApptemplatePath); 
				} 	
				if (is_dir($templatePath)) //application template
				{
					$this->twig->addPath($templatePath);
				}
				
				if (is_dir($pluginPath))// plug in templates
				{
					$dirArray = scandir($pluginPath);
					for ($i=2; $i < sizeof($dirArray); $i++)
					{
						$pluginTemplatePath	=$pluginPath . $dirArray[$i] . '/' . 'template/';
						$pluginWidgetPath	=$pluginPath . $dirArray[$i] . '/' . 'widget/';
						if(is_dir($pluginTemplatePath))
						{
							$this->twig->addPath($pluginTemplatePath);

							$this->checkForWidget($pluginWidgetPath);
						}
					}
				}
			}
			
			//Init all the other things.
			$this->URI=URI::getContainerInstance($classRef,$this->URIBinding);
			
			/*
			 If the URI binding is not the default root binding,
			 then grab the global URI Plugin instance and compare it.
			 Should we find it is not a match, we abort execution of this
			 application.
			 */
			if ($URIBinding!==self::DEFAULT_URI_BINDING)
			{
				$URI=URI::getInstance();
				
				$parts=new Vector(explode('/',$URIBinding));
				if ($parts->get(0)==='')
				{
					$parts->removeKey(0);
				}
				if (!$parts->isEmpty() && $parts->get($parts->count()-1)==='')
				{
					$parts->pop();
				}
				for ($i=0,$j=count($parts); $i<$j; $i++)
				{
					if ($parts[$i]!==$URI->getPart($i))
					{
						//This is a fail. Exit out of this application.
						return;
					}
				}
			}
			$this->request=Request::getInstance();
			
			$this->getDispatcher()	->bindConfig($this->config)
									->bindURI($this->URI);
			
			$this->router=Router::getInstance
			(
				get_class($this),
				$this->URI,
				$this->config
			);
			
			//Ignore the favicon.
			if ($this->URI->getLast()=='favicon.ico')
			{
				exit();
			}
			try
			{
				$this->connectDataSources();
				if (!is_null($this->request->getServer('REMOTE_HOST'))
				|| !is_null($this->request->getServer('HTTP_HOST')))
				{
					$session=ProviderManager::request
					(
						'session::'.($this->config->get('session.type') ?? self::DEFAULT_SESSION_TYPE),
						$this->config->get('session')
					);
					if (is_null($session))
					{
						throw new \Exception(sprintf('I could not find a session provider for "session::%s".',$this->config->get('session.type')));
					}
					invariant($session instanceof CommonSessionInterface, 'Must be a CommonSessionInterface.');
					$this->session=$session;
				}
			}
			catch (\Exception $exception)
			{
				if ($this->config->get('application.mode')==self::MODE_DEVELOPMENT)
				if ($this->config->get('application.debug'))
				{
					debug()->handleException($exception);
				}
			}
		}
		
		//TODO: Migrate this to Intaglio.
		private function checkForWidget (string $pluginWidgetPath):void
		{
			if (is_dir($pluginWidgetPath))
			{
				$dirArray = scandir($pluginWidgetPath);
				for ($i=2; $i < sizeof($dirArray); $i++)
				{
					$pluginWidgetTemplatePath = $pluginWidgetPath . $dirArray[$i] . '/' . 'template/';
					if (is_dir($pluginWidgetTemplatePath))
					{
						$this->twig->addPath($pluginWidgetTemplatePath);
					}
				}
			}
		}
		
		public function initKernal():void
		{
			try
			{
				$this->kernel=new HttpKernel
				(
					shape
					(
						'classLoader'		=>ClassLoader::getInstance(),
						'classManager'		=>ClassManager::getInstance(),
						'eventManager'		=>EventManager::getInstance(),
						'exceptionHandler'	=>new ExceptionBootstrapper()
					)
				);
				$this->nuclio=new Nuclio($this->kernel);
			}
			catch (\Exception $exception)
			{
				var_dump($exception);
				exit();
			}
		}
		
		public function connectDataSources():void
		{
			$connections=$this->config->get('database.connections');
			$manager=DataSourceManager::getInstance();
			if ($connections instanceof KeyedTraversable)
			{
				foreach ($connections as $connectionRef=>$connection)
				{
					$manager->createConnection($connectionRef,new Map($connection));
				}
			}
		}
		
		public function getRequest():Request
		{
			return $this->request;
		}
		
		public function getURI():URI
		{
			return $this->URI;
		}
		
		public function getConfig():Config
		{
			return $this->config;
		}
		
		public function getRouter():Router
		{
			return $this->router;
		}
		
		public function getDispatcher()
		{
			return $this->kernel->getEventManager()->getDispatcher();
		}
		
		public function getSession():CommonSessionInterface
		{
			return $this->session;
		}
	}
}
